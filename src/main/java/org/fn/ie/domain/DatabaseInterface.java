package org.fn.ie.domain;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.fn.ie.repository.*;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;

public class DatabaseInterface {
    private static DatabaseInterface instance;

    public static DatabaseInterface getInstance() {
        if (instance == null)
            instance = new DatabaseInterface();
        return instance;
    }

    public ArrayList<Restaurant> fetchRestaurants(int from, int offset) throws SQLException {
        ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
        ArrayList<RestaurantDAO> rests = NikiveryRepository.getInstance().getRestaurants(from, offset);
        for(int i = 0; i < rests.size(); i++) {
            Restaurant newRestaurant = new Restaurant();
            newRestaurant.setId(rests.get(i).getId());
            newRestaurant.setName(rests.get(i).getName());
            newRestaurant.setLocation(rests.get(i).getLocation());
            newRestaurant.setLogo(rests.get(i).getLogo());
            ArrayList<FoodDAO> menuDAO = NikiveryRepository.getInstance().getRestaurantMenu(newRestaurant.getId());
            ArrayList<Food> menu = new ArrayList<Food>();
            for (int j = 0; j < menuDAO.size(); j++)
            {
                Food newFood = new Food();
                newFood.setName(menuDAO.get(j).getName());
                newFood.setDescription(menuDAO.get(j).getDescription());
                newFood.setImage(menuDAO.get(j).getImage());
                newFood.setPopularity(menuDAO.get(j).getPopularity());
                newFood.setPrice(menuDAO.get(j).getPrice());
                newFood.setRestaurantName(newRestaurant.getName());
                menu.add(newFood);
            }
            newRestaurant.setMenu(menu);
            ArrayList<PartyFoodDAO> partyMenuDAO = NikiveryRepository.getInstance().getPartyFood(newRestaurant.getId());
            ArrayList<PartyFood> partyMenu = new ArrayList<PartyFood>();
            for (int j = 0; j < partyMenuDAO.size(); j++)
            {
                PartyFood newFood = new PartyFood();
                newFood.setName(partyMenuDAO.get(j).getName());
                newFood.setDescription(partyMenuDAO.get(j).getDescription());
                newFood.setImage(partyMenuDAO.get(j).getImage());
                newFood.setPopularity(partyMenuDAO.get(j).getPopularity());
                newFood.setPrice(partyMenuDAO.get(j).getPrice());
                newFood.setCount(partyMenuDAO.get(j).getCount());
                newFood.setOldPrice(partyMenuDAO.get(j).getOldPrice());
                partyMenu.add(newFood);
            }
            newRestaurant.setPartyMenu(partyMenu);
            restaurants.add(newRestaurant);
        }
        return restaurants;
    }

    public Restaurant fetchRestaurant(String resId) throws SQLException {
        Restaurant restaurant = new Restaurant();
        RestaurantDAO rest = NikiveryRepository.getInstance().getRestaurant(resId);
        restaurant.setId(rest.getId());
        restaurant.setName(rest.getName());
        restaurant.setLocation(rest.getLocation());
        restaurant.setLogo(rest.getLogo());
        ArrayList<FoodDAO> menuDAO = NikiveryRepository.getInstance().getRestaurantMenu(restaurant.getId());
        ArrayList<Food> menu = new ArrayList<Food>();
        for (int j = 0; j < menuDAO.size(); j++)
        {
            Food newFood = new Food();
            newFood.setName(menuDAO.get(j).getName());
            newFood.setDescription(menuDAO.get(j).getDescription());
            newFood.setImage(menuDAO.get(j).getImage());
            newFood.setPopularity(menuDAO.get(j).getPopularity());
            newFood.setPrice(menuDAO.get(j).getPrice());
            newFood.setRestaurantName(restaurant.getName());
            menu.add(newFood);
        }
        restaurant.setMenu(menu);
        ArrayList<PartyFoodDAO> partyMenuDAO = NikiveryRepository.getInstance().getPartyFood(restaurant.getId());
        ArrayList<PartyFood> partyMenu = new ArrayList<PartyFood>();
        for (int j = 0; j < partyMenuDAO.size(); j++)
        {
            PartyFood newFood = new PartyFood();
            newFood.setName(partyMenuDAO.get(j).getName());
            newFood.setDescription(partyMenuDAO.get(j).getDescription());
            newFood.setImage(partyMenuDAO.get(j).getImage());
            newFood.setPopularity(partyMenuDAO.get(j).getPopularity());
            newFood.setPrice(partyMenuDAO.get(j).getPrice());
            newFood.setCount(partyMenuDAO.get(j).getCount());
            newFood.setOldPrice(partyMenuDAO.get(j).getOldPrice());
            partyMenu.add(newFood);
        }
        restaurant.setPartyMenu(partyMenu);
        return restaurant;
    }

    public ArrayList<PartyRestaurant> fetchPartyFoodReses(ArrayList<Restaurant> restaurants) throws SQLException {
        ArrayList<String> partyResIds = NikiveryRepository.getInstance().getPartyResIds();
        ArrayList<PartyRestaurant> partyFoodReses = new ArrayList<PartyRestaurant>();
        for (int i = 0; i < partyResIds.size(); i++) {
            for (int j = 0; j < restaurants.size(); j++) {
                if (partyResIds.get(i).equals(restaurants.get(j))) {
                    PartyRestaurant newRestaurant = new PartyRestaurant();
                    newRestaurant.setId(restaurants.get(j).getId());
                    newRestaurant.setName(restaurants.get(j).getName());
                    newRestaurant.setLogo(restaurants.get(j).getLogo());
                    newRestaurant.setLocation(restaurants.get(j).getLocation());
                    ArrayList<PartyFoodDAO> menuDAO = NikiveryRepository.getInstance().getPartyFood(restaurants.get(j).getId());
                    ArrayList<PartyFood> menu = new ArrayList<PartyFood>();
                    for (int k = 0; k < menuDAO.size(); k++)
                    {
                        PartyFood newFood = new PartyFood();
                        newFood.setName(menuDAO.get(k).getName());
                        newFood.setDescription(menuDAO.get(k).getDescription());
                        newFood.setImage(menuDAO.get(k).getImage());
                        newFood.setPopularity(menuDAO.get(k).getPopularity());
                        newFood.setPrice(menuDAO.get(k).getPrice());
                        newFood.setCount(menuDAO.get(k).getCount());
                        newFood.setOldPrice(menuDAO.get(k).getOldPrice());
                        menu.add(newFood);
                    }
                    newRestaurant.setMenu(menu);
                }
            }
        }
        return partyFoodReses;
    }

    public UserProfile fetchUserProfile(int userId) throws SQLException {
        UserProfile userProfile = new UserProfile();
        UserProfileDAO userDAO = NikiveryRepository.getInstance().getUserProfile(userId);
        userProfile.setId(userId);
        userProfile.setFirstName(userDAO.getFirstName());
        userProfile.setLastName(userDAO.getLastName());
        userProfile.setEmail(userDAO.getEmail());
        userProfile.setPhoneNumber(userDAO.getPhoneNumber());
        userProfile.setCredit(userDAO.getCredit());
        userProfile.setCartRestaurant(userDAO.getCartRestaurant());
        ArrayList<FoodDAO> userCartDAO = NikiveryRepository.getInstance().getCart(userProfile.getId());
        ArrayList<Food> userCart = new ArrayList<Food>();
        for (int j = 0; j < userCartDAO.size(); j++)
        {
            Food newFood = new Food();
            newFood.setName(userCartDAO.get(j).getName());
            newFood.setDescription(userCartDAO.get(j).getDescription());
            newFood.setImage(userCartDAO.get(j).getImage());
            newFood.setPopularity(userCartDAO.get(j).getPopularity());
            newFood.setPrice(userCartDAO.get(j).getPrice());
            userCart.add(newFood);
        }
        userProfile.setCart(userCart);
        int numOfFactors = NikiveryRepository.getInstance().getNumOfFactors(userProfile.getId());
        ArrayList<Factor> factors = new ArrayList<Factor>();
        ArrayList<OrderStatus> statuses = new ArrayList<OrderStatus>();
        for (int i = 0; i < numOfFactors; i++) {
            Factor factor = new Factor();
            FactorDAO factorDAO = NikiveryRepository.getInstance().getFactor(userProfile.getId(), i);
            factor.setNetPrice(factorDAO.getNetPrice());
            factor.setRestaurantName(factorDAO.getRestaurantName());
            factor.setOrders(factorDAO.getOrders());
            factors.add(factor);
            statuses.add(NikiveryRepository.getInstance().getOrderStatus(userProfile.getId(), i));
        }
        userProfile.setFactors(factors);
        userProfile.setDeliveryStates(statuses);
        userProfile.checkValues();
        return userProfile;
    }

    public void updateRestaurants(ArrayList<Restaurant> restaurants) {
        NikiveryRepository.getInstance().batchUpdateRestaurants(restaurants);
    }

    public void addRestaurant(Restaurant restaurant) {
        NikiveryRepository.getInstance().addRestaurant(restaurant);
        ArrayList<Food> menu = restaurant.getMenu();
        for(int j = 0; j < menu.size(); j++) {
            NikiveryRepository.getInstance().addFood(restaurant.getId(), restaurant.getName(),
                    menu.get(j));
        }
        ArrayList<PartyFood> partyMenu = restaurant.getPartyMenu();
        for(int j = 0; j < partyMenu.size(); j++) {
            NikiveryRepository.getInstance().addPartyFood(restaurant.getId(), partyMenu.get(j));
        }
    }

    public void addFactor(int userId, int factorId, Factor factor, String resId) {
        NikiveryRepository.getInstance().addFactor(userId, factorId, factor.getRestaurantName(), factor.getNetPrice());
        for (int i = 0; i < factor.getOrders().size(); i++) {
            NikiveryRepository.getInstance().addFactorItem(userId, factorId, resId, factor.getOrders().get(i).getFoodName(),
                    factor.getOrders().get(i).getCount());
        }
    }

    public int addUser(String firstName, String lastName, String password, String phone, String email) {
        return NikiveryRepository.getInstance().addUser(firstName, lastName, password.hashCode(), phone, email);
    }

    public void updateCredit(int userId, int credit) {
        NikiveryRepository.getInstance().setCredit(credit, userId);
    }

    public void updateCart(int userId, String resId, ArrayList<Food> cart) {
        NikiveryRepository.getInstance().removeCartItems(userId);
        for(int i = 0; i < cart.size(); i++) {
            NikiveryRepository.getInstance().addCartItem(userId, i, resId, cart.get(i).getName(), cart.get(i).getPrice());
        }
    }

    public void setCartRestaurant(int userId, String cartResId) {
        NikiveryRepository.getInstance().setUserCartRestaurant(userId, cartResId);
    }

    public void decreasePartyFoodCount(String foodName, String cartRestaurant, int count) {
        NikiveryRepository.getInstance().decreasePartyFoodCount(foodName, cartRestaurant, count);
    }

    public void addDeliveryState(int status, int orderId, int userId, String resName) {
        NikiveryRepository.getInstance().addOrderStatus(userId, orderId, status, resName);
    }

    public void setDeliveryState(int status, int orderId, int userId) {
        NikiveryRepository.getInstance().setDeliveryState(status, orderId, userId);
    }

    public void removeFoodParty() {
        NikiveryRepository.getInstance().removeFoodParty();
    }

    public void removeCartItems(int userId) {
        NikiveryRepository.getInstance().removeCartItems(userId);
    }

    public ArrayList<Restaurant> getSearchRestaurants(String foodName, String resName, int from, int offset) throws SQLException {
        ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
        ArrayList<RestaurantDAO> rests = NikiveryRepository.getInstance().getSearchRestaurants(foodName, resName, from, offset);
        for(int i = 0; i < rests.size(); i++) {
            Restaurant newRestaurant = new Restaurant();
            newRestaurant.setId(rests.get(i).getId());
            newRestaurant.setName(rests.get(i).getName());
            newRestaurant.setLocation(rests.get(i).getLocation());
            newRestaurant.setLogo(rests.get(i).getLogo());
            restaurants.add(newRestaurant);
        }
        return restaurants;
    }

    public String login(String email, String password) {
        int userId = -1;
        try {
            userId = NikiveryRepository.getInstance().login(email, password.hashCode());
            if(userId != -1) {
                return JWT.getInstance().createToken(userId);
            }
            else {
                return "";
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String googleLogin(String googleToken) {
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new JacksonFactory())
                .setAudience(Collections.singletonList("851108352744-s94af4dv4akkc08mcahfduc1i4r87370.apps.googleusercontent.com"))
                .build();

        GoogleIdToken idToken = null;
        try {
            idToken = verifier.verify(googleToken);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            return "";
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
        if (idToken != null) {
            Payload payload = idToken.getPayload();

            String email = payload.getEmail();
            System.out.println(email);
            int userId = -1;
            try {
                userId = NikiveryRepository.getInstance().googleLogin(email);
                if(userId != -1) {
                    return JWT.getInstance().createToken(userId);
                }
                else {
                    return "";
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        } else {
            System.out.println("Invalid ID token.");
        }
        return "";
    }
}
