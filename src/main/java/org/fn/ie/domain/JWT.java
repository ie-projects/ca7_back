package org.fn.ie.domain;

import io.jsonwebtoken.*;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.util.Date;

public class JWT {
    private static JWT instance;

    public static JWT getInstance() {
        if (instance == null) {
            instance = new JWT();
        }
        return instance;
    }

    public String createToken(int userId) {
        String jwt = "";
        try {
            jwt = Jwts.builder()
                .setSubject("users/" + userId)
                .setExpiration(new Date(new Date().getTime() + (1000 * 60 * 60 * 24)))
                .setIssuer("loghme_server")
                .setIssuedAt(new Date())
                .claim("userId", Integer.toString(userId))
                .signWith(
                        SignatureAlgorithm.HS256,
                        "loghme".getBytes("UTF-8")
                )
                .compact();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return jwt;
    }

    public int validateSign(String jwt) {
        Jws<Claims> claims = null;
        try {
            claims = Jwts.parser()
            .setSigningKey("loghme".getBytes("UTF-8"))
            .parseClaimsJws(jwt);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            return -1;
        } catch (JwtException e) {
            return -1;
        }
        int userId = Integer.parseInt((String) claims.getBody().get("userId"));
        return userId;
    }
}
