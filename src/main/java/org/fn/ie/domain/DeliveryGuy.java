package org.fn.ie.domain;

public class DeliveryGuy {
    private String id;
    private int velocity;
    private Location location;

    public String getId() { return id; }
    public Location getLocation() { return location; }
    public int getVelocity() { return velocity; }
}
