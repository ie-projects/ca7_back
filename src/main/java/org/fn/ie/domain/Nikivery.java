package org.fn.ie.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.time.StopWatch;
import org.fn.ie.repository.*;
import org.springframework.ejb.interceptor.SpringBeanAutowiringInterceptor;

import javax.xml.crypto.Data;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.*;

public class Nikivery {
    private static Nikivery instance;
    private static StopWatch foodPartyClock = new StopWatch();
    private ArrayList<Restaurant> restaurants = new ArrayList<Restaurant>();
    private ArrayList<DeliveryGuy> deliveryGuys = new ArrayList<DeliveryGuy>();
    private ArrayList<PartyRestaurant> partyFoodReses = new ArrayList<PartyRestaurant>();
    private UserProfile userProfile = new UserProfile();

    private ObjectMapper mapper = new ObjectMapper();

    public static Nikivery getInstance() {
        if(instance == null)
        {
            instance = new Nikivery();
            instance.getRestaurantsFromServer();
            try {
                instance.restaurants = DatabaseInterface.getInstance().fetchRestaurants(0, 8);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println("Restaurants loaded!");
            try {
                instance.partyFoodReses = DatabaseInterface.getInstance().fetchPartyFoodReses(instance.restaurants);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println("Food Party loaded!");
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    DatabaseInterface.getInstance().removeFoodParty();
                    getInstance().getFoodPartyFromServer();
                    ArrayList<Restaurant> updates = new ArrayList<Restaurant>();
                    ArrayList<PartyRestaurant> partyReses = getInstance().partyFoodReses;
                    for (int i = 0; i < partyReses.size(); i++)
                    {
                        if(instance.getRestaurant(partyReses.get(i).getId()) != null)
                        {
                            instance.getRestaurant(partyReses.get(i).getId()).setPartyMenu(partyReses.get(i).getMenu());
                            updates.add(instance.getRestaurant(partyReses.get(i).getId()));
                        }
                        else
                        {
                            instance.addRestaurant(partyReses.get(i));
                            updates.add(instance.restaurants.get(instance.restaurants.size() - 1));
                            updates.get(updates.size() - 1).setPartyMenu(partyReses.get(i).getMenu());
                        }
                    }
                    DatabaseInterface.getInstance().updateRestaurants(updates);
                    foodPartyClock.reset();
                    foodPartyClock.start();
                }
            }, 0, 15 * 60 * 1000);

            System.out.println("Initialization completed!");
        }
        return instance;
    }

    public void getUserProfile(int userId) {
        try {
            instance.userProfile = DatabaseInterface.getInstance().fetchUserProfile(userId);
            System.out.println("User Profile loaded!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public long getFoodPartyClock() { return foodPartyClock.getTime(); }

    public UserProfile getUserProfile() { return userProfile; }

    public void getRestaurantsFromServer()
    {
        String urlString = "http://138.197.181.131:8080/restaurants";
        URL url;
        URLConnection connection;
        InputStream input;
        try {
            url = new URL(urlString);
            connection = url.openConnection();
            input = connection.getInputStream();
            Scanner scanner = new Scanner(input, "utf-8").useDelimiter("\\A");
            String resesJson = scanner.hasNext() ? scanner.next() : "";
            restaurants = mapper.readValue(resesJson, mapper.getTypeFactory().
                    constructCollectionType(List.class, Restaurant.class));
            DatabaseInterface.getInstance().updateRestaurants(restaurants);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDeliveryGuysFromServer()
    {
        String urlString = "http://138.197.181.131:8080/deliveries";
        URL url;
        URLConnection connection;
        InputStream input;
        try {
            url = new URL(urlString);
            connection = url.openConnection();
            input = connection.getInputStream();
            Scanner scanner = new Scanner(input, "utf-8").useDelimiter("\\A");
            String deliesJson = scanner.hasNext() ? scanner.next() : "";
            deliveryGuys = mapper.readValue(deliesJson, mapper.getTypeFactory().
                    constructCollectionType(List.class, DeliveryGuy.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getFoodPartyFromServer()
    {
        String urlString = "http://138.197.181.131:8080/foodparty";
        URL url;
        URLConnection connection;
        InputStream input;
        try {
            url = new URL(urlString);
            connection = url.openConnection();
            input = connection.getInputStream();
            Scanner scanner = new Scanner(input, "utf-8").useDelimiter("\\A");
            String foodsJson = scanner.hasNext() ? scanner.next() : "";
            partyFoodReses = mapper.readValue(foodsJson, mapper.getTypeFactory().
                    constructCollectionType(List.class, PartyRestaurant.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addRestaurant(PartyRestaurant partyRestaurant)
    {
        Restaurant res = new Restaurant();
        res.copyRestaurant(partyRestaurant);
        restaurants.add(res);
    }

    public ArrayList<Restaurant> getRestaurants()
    {
        ArrayList<Restaurant> validRestaurants = new ArrayList<Restaurant>();
        for(Restaurant restaurant : restaurants)
        {
            if(Math.hypot(restaurant.getLocation().getX(), restaurant.getLocation().getY()) <= 170)
                validRestaurants.add(restaurant);
        }
        return validRestaurants;
    }

    public Restaurant getRestaurant(String id)
    {
        for(int i = 0; i < instance.restaurants.size(); i++) {
            if(instance.restaurants.get(i).getId().equals(id)) {
                return instance.restaurants.get(i);
            }
        }
        return null;
    }

    public int addToCart(String foodName, String restaurantId, boolean isParty)
    {
        for(Restaurant restaurant : restaurants)
        {
            if(restaurant.getId().equals(restaurantId))
            {
                if(!isParty)
                    for(Food food : restaurant.getMenu())
                    {
                        if(food.getName().equals(foodName))
                        {
                            if(userProfile.getCart().size() == 0 || userProfile.getCartRestaurant().equals(restaurantId))
                            {
                                userProfile.addFoodToCart(food);
                                userProfile.setCartRestaurant(restaurantId);
                                DatabaseInterface.getInstance().updateCart(userProfile.getId(), restaurantId, userProfile.getRawCart());
                                DatabaseInterface.getInstance().setCartRestaurant(userProfile.getId(), restaurantId);
                                return 0;
                            }
                            else
                            {
                                return -1;
                            }
                        }
                    }
                else
                    for(PartyFood food : restaurant.getPartyMenu())
                    {
                        if(food.getName().equals(foodName))
                        {
                            if(userProfile.getCart().size() == 0 || userProfile.getCartRestaurant().equals(restaurantId))
                            {
                                userProfile.setCartRestaurant(restaurantId);
                                int status = checkPartyFoodCount(foodName, restaurantId);
                                if (status == 1)
                                {
                                    Food foodie = new Food();
                                    foodie.copyFood(food);
                                    userProfile.addFoodToCart(foodie);
                                    DatabaseInterface.getInstance().updateCart(userProfile.getId(), restaurantId, userProfile.getRawCart());
                                    DatabaseInterface.getInstance().setCartRestaurant(userProfile.getId(), restaurantId);
                                    return 0;
                                }
                                else if (status == 0)
                                    return -3;
                            }
                            else
                            {
                                return -1;
                            }
                        }
                    }

            }
        }
        return -2;
    }

    public int finalizeOrder()
    {
        if(userProfile.getCart().size() == 0)
            return -1;
        int netCost = 0;
        ArrayList<Order> orders = userProfile.getCart();
        for(int i = 0; i < orders.size(); i++)
            netCost += orders.get(i).getPrice() * orders.get(i).getCount();
        if(userProfile.getCredit() < netCost)
            return -2;
        for(int i = 0; i < userProfile.getCart().size(); i++)
        {
            for(int j = 0; j < partyFoodReses.size(); j++)
                if(partyFoodReses.get(j).getId().equals(userProfile.getCartRestaurant()))
                {
                    int status = -2;
                    for(int q = 0; q < partyFoodReses.get(j).getMenu().size(); q++)
                        if(partyFoodReses.get(j).getMenu().get(q).getName().equals(userProfile.getCart().get(i).getFoodName())
                                && partyFoodReses.get(j).getMenu().get(q).getPrice() == userProfile.getCart().get(i).getPrice())
                        {
                            status = decreasePartyFoodCount(userProfile.getCart().get(i).getFoodName(),
                                    userProfile.getCartRestaurant(), userProfile.getCart().get(i).getCount());
                            if (status == 0) {
                                DatabaseInterface.getInstance().decreasePartyFoodCount(userProfile.getCart().get(i).getFoodName(),
                                        userProfile.getCartRestaurant(), userProfile.getCart().get(i).getCount());
                            }
                        }

                    if(status == -1)
                        return -3;
                }
        }
        userProfile.addCredit(-1 * netCost);
        userProfile.addToFactors(userProfile.getCartRestaurant(), orders, netCost);
        userProfile.emptyCart();
        DatabaseInterface.getInstance().addFactor(userProfile.getId(), userProfile.getFactors().size()-1,
                userProfile.getFactors().get(userProfile.getFactors().size()-1), userProfile.getCartRestaurant());
        assignDeliveryGuy(userProfile);
        return 0;
    }

    public void assignDeliveryGuy(UserProfile user)
    {
        Timer timer = new Timer();
        userProfile.setDeliveryState(0, -1);
        int orderId = user.getDeliveryStates().size() - 1;
        for(int i = 0; i < restaurants.size(); i++)
            if(restaurants.get(i).getId().equals(userProfile.getCartRestaurant())) {
                DatabaseInterface.getInstance().addDeliveryState(0, orderId, userProfile.getId(),
                        restaurants.get(i).getName());
                break;
            }

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getDeliveryGuysFromServer();
                if(deliveryGuys.size() > 0)
                {
                    timer.cancel();
                    timer.purge();
                    for (int i = 0; i < restaurants.size(); i++)
                        if (restaurants.get(i).getId().equals(user.getCartRestaurant()))
                            selectDeliveryGuy(restaurants.get(i), orderId);
                }
            }
        }, 30*1000, 30*1000);

        deliveryGuys = new ArrayList<DeliveryGuy>();
    }

    private void selectDeliveryGuy(Restaurant restaurant, int orderId)
    {
        int minIndex = 0;
        Timer timer = new Timer();
        double minTime = (Math.hypot(deliveryGuys.get(0).getLocation().getX() - restaurant.getLocation().getX(),
                deliveryGuys.get(0).getLocation().getY() - restaurant.getLocation().getY()) +
                Math.hypot(restaurant.getLocation().getX(), restaurant.getLocation().getY()))/ deliveryGuys.get(0).getVelocity();
        if(deliveryGuys.size() > 1)
            for(int i = 1; i < deliveryGuys.size(); i++)
            {
                double candidateTime = (Math.hypot(deliveryGuys.get(i).getLocation().getX() - restaurant.getLocation().getX(),
                        deliveryGuys.get(i).getLocation().getY() - restaurant.getLocation().getY()) +
                        Math.hypot(restaurant.getLocation().getX(), restaurant.getLocation().getY()))/deliveryGuys.get(i).getVelocity();
                if(candidateTime < minTime)
                {
                    minIndex = i;
                    minTime = candidateTime;
                }
            }
        System.out.println("Delivery guy number " + minIndex + " is chosen!");
        userProfile.setDeliveryState(1, orderId);
        DatabaseInterface.getInstance().setDeliveryState(1, orderId, userProfile.getId());
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                userProfile.setDeliveryState(2, orderId);
                DatabaseInterface.getInstance().setDeliveryState(2, orderId, userProfile.getId());
            }
        }, 30*1000);
    }

    public ArrayList<PartyRestaurant> getPartyFoodReses() { return partyFoodReses; }

    private int decreasePartyFoodCount(String foodName, String restaurantId, int count)
    {
        for(int i = 0; i < partyFoodReses.size(); i++)
            if(partyFoodReses.get(i).getId().equals(restaurantId))
                for(int j = 0; j < partyFoodReses.get(i).getMenu().size(); j++)
                    if(partyFoodReses.get(i).getMenu().get(j).getName().equals(foodName))
                        return partyFoodReses.get(i).getMenu().get(j).decreaseCount(count);
        return -2;
    }

    private int checkPartyFoodCount(String foodName, String restaurantId)
    {
        for(int i = 0; i < partyFoodReses.size(); i++)
            if(partyFoodReses.get(i).getId().equals(restaurantId))
                for(int j = 0; j < partyFoodReses.get(i).getMenu().size(); j++)
                    if(partyFoodReses.get(i).getMenu().get(j).getName().equals(foodName))
                    {
                        int count = partyFoodReses.get(i).getMenu().get(j).getCount();
                        if (count > 0)
                            return 1;
                        return 0;
                    }
        return -2;
    }

    public int incrementInCart(String foodName)
    {
        for(int i = 0; i < Nikivery.getInstance().getUserProfile().getRawCart().size(); i++)
            if(Nikivery.getInstance().getUserProfile().getRawCart().get(i).getName().equals(foodName))
            {
                Food food = new Food(Nikivery.getInstance().getUserProfile().getRawCart().get(i));
                Nikivery.getInstance().getUserProfile().addFoodToCart(food);
                NikiveryRepository.getInstance().addCartItem(Nikivery.getInstance().getUserProfile().getId(),
                        Nikivery.getInstance().getUserProfile().getRawCart().size() - 1,
                        Nikivery.getInstance().userProfile.getCartRestaurant(), food.getName(), food.getPrice());
                return 0;
            }
        return -1;
    }

    public int decrementInCart(String foodName)
    {
        for(int i = Nikivery.getInstance().getUserProfile().getRawCart().size() - 1; i >= 0 ; i--)
            if(Nikivery.getInstance().getUserProfile().getRawCart().get(i).getName().equals(foodName))
            {
                Nikivery.getInstance().getUserProfile().removeFoodFromCart(i);
                NikiveryRepository.getInstance().removeCartItem(Nikivery.getInstance().getUserProfile().getId(), i);
                return 0;
            }
        return -1;
    }

    public ArrayList<Restaurant> search(String foodName, String resName, int from, int offset) throws SQLException {
        return DatabaseInterface.getInstance().getSearchRestaurants(foodName, resName, from, offset);
    }
}