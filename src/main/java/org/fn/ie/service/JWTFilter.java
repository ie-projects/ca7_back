package org.fn.ie.service;
import org.fn.ie.domain.JWT;
import org.fn.ie.domain.Nikivery;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ WebFilter(asyncSupported = true, urlPatterns = { "/[^(login)(signup)(inside_check)]" })
public class JWTFilter implements Filter {
    public JWTFilter() {
    }
    public void destroy() {
        // TODO Auto-generated method stub
    }
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if(request.getRequestURI().startsWith("/login") || request.getRequestURI().startsWith("/signup") ||
                request.getRequestURI().startsWith("/inside_check") || request.getRequestURI().startsWith("/google")) {
            chain.doFilter(request, servletResponse);
        }
        else {
            String jwt = request.getHeader("Authorization");
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            if(jwt != null) {
                int id = JWT.getInstance().validateSign(jwt);
                if (id < 0) {
                    resp.setStatus(403);
                    resp.sendError(403);
                    request.setAttribute("userId", -1);
                }
                else {
//                Nikivery.getInstance().getUserProfile(id);
                    request.setAttribute("userId", id);
                }
            }
            else {
                request.setAttribute("userId", -1);
                resp.setStatus(401);
                resp.sendError(401);
            }
            chain.doFilter(request, servletResponse);
        }

    }
    public void init(FilterConfig fConfig) throws ServletException {
        // TODO Auto-generated method stub
    }
}