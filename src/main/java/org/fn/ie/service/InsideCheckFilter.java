package org.fn.ie.service;
import org.fn.ie.domain.JWT;
import org.fn.ie.domain.Nikivery;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ WebFilter(asyncSupported = true, urlPatterns = { "/inside_check" })
public class InsideCheckFilter implements Filter {
    public InsideCheckFilter() {
    }
    public void destroy() {
        // TODO Auto-generated method stub
    }
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if(request.getRequestURI().startsWith("/inside_check")) {
            String jwt = request.getHeader("Authorization");
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            if(jwt != null) {
                int id = JWT.getInstance().validateSign(jwt);
                request.setAttribute("userId", id);
            }
            else {
                request.setAttribute("userId", -1);
            }
        }
        chain.doFilter(request, servletResponse);
    }
    public void init(FilterConfig fConfig) throws ServletException {
        // TODO Auto-generated method stub
    }
}