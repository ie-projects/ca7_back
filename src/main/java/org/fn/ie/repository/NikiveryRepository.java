package org.fn.ie.repository;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.fn.ie.domain.*;

import java.sql.*;
import java.util.ArrayList;

public class NikiveryRepository {
    private static NikiveryRepository instance;

    ComboPooledDataSource dataSource;

    private NikiveryRepository() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        dataSource = new ComboPooledDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/loghme?useSSL=false");
        dataSource.setUser("root");
        dataSource.setPassword("13771375");

        dataSource.setInitialPoolSize(5);
        dataSource.setMinPoolSize(5);
        dataSource.setAcquireIncrement(5);
        dataSource.setMaxPoolSize(20);
        dataSource.setMaxStatements(100);
    }

    public static NikiveryRepository getInstance() {
        if (instance == null)
            instance = new NikiveryRepository();
        return instance;
    }

    public ArrayList<RestaurantDAO> getRestaurants(int from, int offset) throws SQLException {
        ArrayList<RestaurantDAO> restaurants = new ArrayList<RestaurantDAO>();
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select * from restaurant limit ? , ? ;");
        statement.setInt(1, from);
        statement.setInt(2, offset);
        ResultSet result = statement.executeQuery();
        while(result.next()) {
            RestaurantDAO restaurantDAO = new RestaurantDAO();
            restaurantDAO.setId(result.getString("id"));
            restaurantDAO.setName(result.getString("name"));
            Location resLoc = new Location();
            resLoc.setX(result.getInt("locx"));
            resLoc.setY(result.getInt("locy"));
            restaurantDAO.setLocation(resLoc);
            restaurantDAO.setLogo(result.getString("logo"));
            restaurants.add(restaurantDAO);
        }
        result.close();
        statement.close();
        connection.close();
        return restaurants;
    }

    public RestaurantDAO getRestaurant(String resId) throws SQLException {
        RestaurantDAO restaurantDAO = new RestaurantDAO();
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select R.* from restaurant R where R.id = ? ;");
        statement.setString(1, resId);
        ResultSet result = statement.executeQuery();
        if(result.next()) {
            restaurantDAO.setId(result.getString("id"));
            restaurantDAO.setName(result.getString("name"));
            Location resLoc = new Location();
            resLoc.setX(result.getInt("locx"));
            resLoc.setY(result.getInt("locy"));
            restaurantDAO.setLocation(resLoc);
            restaurantDAO.setLogo(result.getString("logo"));
        }
        result.close();
        statement.close();
        connection.close();
        return restaurantDAO;
    }

    public ArrayList<FoodDAO> getRestaurantMenu(String resId) throws SQLException {
        ArrayList<FoodDAO> menu = new ArrayList<FoodDAO>();
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select * from food F where F.resId = ? ;");
        statement.setString(1, resId);
        ResultSet result = statement.executeQuery();
        while(result.next()) {
            FoodDAO foodDAO = new FoodDAO();
            foodDAO.setName(result.getString("name"));
            foodDAO.setDescription(result.getString("description"));
            foodDAO.setPopularity(result.getFloat("popularity"));
            foodDAO.setPrice(result.getInt("price"));
            foodDAO.setImage(result.getString("image"));
            foodDAO.setRestaurantName(result.getString("resName"));
            menu.add(foodDAO);
        }
        result.close();
        statement.close();
        connection.close();
        return menu;
    }

    public ArrayList<PartyFoodDAO> getPartyFood(String resId) throws SQLException {
        ArrayList<PartyFoodDAO> menu = new ArrayList<PartyFoodDAO>();
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select * from partyfood F ;");
        PreparedStatement statement2 = connection.prepareStatement("select * from restaurant R where R.id = ? ;");
        ResultSet result = statement.executeQuery();
        while(result.next()) {
            PartyFoodDAO partyFoodDAO = new PartyFoodDAO();
            partyFoodDAO.setCount(result.getInt("count"));
            partyFoodDAO.setOldPrice(result.getInt("oldPrice"));
            partyFoodDAO.setName(result.getString("name"));
            partyFoodDAO.setDescription(result.getString("description"));
            partyFoodDAO.setPopularity(result.getFloat("popularity"));
            partyFoodDAO.setPrice(result.getInt("price"));
            partyFoodDAO.setImage(result.getString("image"));
            partyFoodDAO.setRestaurantId(result.getString("resId"));
            statement2.setString(1, partyFoodDAO.getRestaurantId());
            ResultSet restaurantResult = statement2.executeQuery();
            if(restaurantResult.next()) {
                partyFoodDAO.setRestaurantName(restaurantResult.getString("name"));
            }
            menu.add(partyFoodDAO);
            restaurantResult.close();
        }
        statement2.close();
        result.close();
        statement.close();
        connection.close();
        return menu;
    }

    public UserProfileDAO getUserProfile(int id) throws SQLException {
        UserProfileDAO userProfileDAO = new UserProfileDAO();
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select * from userprofile U where U.id = ? ;");
        statement.setInt(1, id);
        ResultSet result = statement.executeQuery();
        if(result.next()) {
            userProfileDAO.setId(result.getInt("id"));
            userProfileDAO.setFirstName(result.getString("firstname"));
            userProfileDAO.setLastName(result.getString("lastname"));
            userProfileDAO.setPhoneNumber(result.getString("phonenumber"));
            userProfileDAO.setEmail(result.getString("email"));
            userProfileDAO.setCredit(result.getInt("credit"));
            userProfileDAO.setCartRestaurant(result.getString("cartrestaurant"));
        }
        result.close();
        statement.close();
        connection.close();
        return userProfileDAO;
    }

    public FactorDAO getFactor(int userId, int factorId) throws SQLException {
        FactorDAO factor = new FactorDAO();
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select * from factor F where F.userId = ? and F.id = ? ;");
        statement.setInt(1, userId);
        statement.setInt(2, factorId);
        PreparedStatement statement3 = connection.prepareStatement("select * from factoritem I where I.userId = ? and I.id = ? ;");
        PreparedStatement statement2 = connection.prepareStatement("select * from food FO where FO.resId = ? and FO.name = ? ;");
        ResultSet result = statement.executeQuery();
        ArrayList<Order> orders = new ArrayList<Order>();
        if(result.next()) {
            factor.setRestaurantName(result.getString("resName"));
            factor.setNetPrice(result.getInt("netPrice"));
        }
        result.close();
        statement3.setInt(1, userId);
        statement3.setInt(2, factorId);
        ResultSet itemsResult = statement3.executeQuery();
        while(itemsResult.next()) {
            Order order = null;
            statement2.setString(1, itemsResult.getString("resId"));
            statement2.setString(2, itemsResult.getString("foodName"));
            ResultSet priceResult = statement2.executeQuery();
            while(priceResult.next()) {
                order = new Order(itemsResult.getString("foodName"), itemsResult.getInt("count"),
                        priceResult.getInt("price"));
            }
            priceResult.close();
            if(order != null)
                orders.add(order);
        }
        itemsResult.close();
        factor.setOrders(orders);
        statement.close();
        statement3.close();
        statement2.close();
        connection.close();
        return factor;
    }

    public OrderStatus getOrderStatus(int userId, int id) throws SQLException {
        OrderStatus orderStatus = null;
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select * from orderstatus U where U.userId = ? and U.id = ? ;");
        statement.setInt(1, userId);
        statement.setInt(2, id);
        ResultSet result = statement.executeQuery();
        if(result.next()) {
            orderStatus = new OrderStatus(result.getInt("status"), result.getString("resName"));
        }
        result.close();
        statement.close();
        connection.close();
        return orderStatus;
    }

    public ArrayList<FoodDAO> getCart(int userId) throws SQLException {
        ArrayList<FoodDAO> cart = new ArrayList<FoodDAO>();
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select * from cartitem C where C.userId = ? ;");
        statement.setInt(1, userId);
        PreparedStatement statement2 = connection.prepareStatement("select * from food F where F.resId = ? " +
                "and F.name = ? ;");
        ResultSet result = statement.executeQuery();
        while(result.next()) {
            statement2.setString(1, result.getString("resId"));
            statement2.setString(2, result.getString("foodName"));
            ResultSet FoodResult = statement2.executeQuery();
            if(FoodResult.next()) {
                FoodDAO foodDAO = new FoodDAO();
                foodDAO.setName(result.getString("foodName"));
                foodDAO.setDescription(FoodResult.getString("description"));
                foodDAO.setPopularity(FoodResult.getFloat("popularity"));
                foodDAO.setPrice(result.getInt("price"));
                foodDAO.setImage(FoodResult.getString("image"));
                foodDAO.setRestaurantName(FoodResult.getString("resName"));
                cart.add(foodDAO);
            }
            FoodResult.close();
        }
        result.close();
        statement.close();
        connection.close();
        return cart;
    }

    public ArrayList<String> getPartyResIds() throws SQLException {
        ArrayList<String> partyResIds = new ArrayList<String>();
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select distinct F.resId as resId from partyfood F;");
        ResultSet result = statement.executeQuery();
        while(result.next()) {
            partyResIds.add(result.getString("resId"));
        }
        result.close();
        statement.close();
        connection.close();
        return partyResIds;
    }

    public int getNumOfFactors(int userId) throws SQLException {
        int count = 0;
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select count(*) as cnt from factor F where F.userId = ? ;");
        statement.setInt(1, userId);
        ResultSet result = statement.executeQuery();
        if(result.next()) {
            count = result.getInt("cnt");
        }
        result.close();
        statement.close();
        connection.close();
        return count;
    }

    public void addRestaurant(Restaurant restaurant) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert ignore into restaurant (id, name, locx, locy, logo) values (?, ?, ?, ?, ?);");
            pStatement.setString(1, restaurant.getId());
            pStatement.setString(2, restaurant.getName());
            pStatement.setInt(3, restaurant.getLocation().getX());
            pStatement.setInt(4, restaurant.getLocation().getY());
            pStatement.setString(5, restaurant.getLogo());
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addFood(String resId, String resName, Food food) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert ignore into food (resId, name, description, popularity, price, image, resName) values (?, ?, ?, ?, ?, ?, ?);");
            pStatement.setString(1, resId);
            pStatement.setString(2, food.getName());
            pStatement.setString(3, food.getDescription());
            pStatement.setFloat(4, food.getPopularity());
            pStatement.setInt(5, food.getPrice());
            pStatement.setString(6, food.getImage());
            pStatement.setString(7, resName);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addPartyFood(String resId, PartyFood food) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert ignore into partyfood (resId, name, description, popularity, price, image, count, oldPrice) " +
                            "values (?, ?, ?, ?, ?, ?, ?, ?);");
            pStatement.setString(1, resId);
            pStatement.setString(2, food.getName());
            pStatement.setString(3, food.getDescription());
            pStatement.setFloat(4, food.getPopularity());
            pStatement.setInt(5, food.getPrice());
            pStatement.setString(6, food.getImage());
            pStatement.setInt(7, food.getCount());
            pStatement.setInt(8, food.getOldPrice());
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addFactor(int userId, int id, String resName, int netPrice) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert into factor (userId, id, resName, netPrice) " +
                            "values (?, ?, ?, ?);");
            pStatement.setInt(1, userId);
            pStatement.setInt(2, id);
            pStatement.setString(3, resName);
            pStatement.setInt(4, netPrice);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addFactorItem(int userId, int id, String resId, String foodName, int count) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert into factoritem (userId, id, resId, foodName, count) " +
                            "values (?, ?, ?, ?, ?);");
            pStatement.setInt(1, userId);
            pStatement.setInt(2, id);
            pStatement.setString(3, resId);
            pStatement.setString(4, foodName);
            pStatement.setInt(5, count);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addOrderStatus(int userId, int id, int status, String resName) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert into orderstatus (userId, id, status, resName) " +
                            "values (?, ?, ?, ?);");
            pStatement.setInt(1, userId);
            pStatement.setInt(2, id);
            pStatement.setInt(3, status);
            pStatement.setString(4, resName);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addCartItem(int userId, int id, String resId, String foodName, int price) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert into cartitem (userId, id, resId, foodName, price) " +
                            "values (?, ?, ?, ?, ?);");
            pStatement.setInt(1, userId);
            pStatement.setInt(2, id);
            pStatement.setString(3, resId);
            pStatement.setString(4, foodName);
            pStatement.setInt(5, price);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int addUser(String firstName, String lastName, int password, String phone, String email) {
        Connection connection;
        int id = 0;
        try {
            connection = dataSource.getConnection();
            PreparedStatement statement1 = connection.prepareStatement("select U.email as email from userprofile U where " +
                    "U.email = ? ;");
            statement1.setString(1, email);
            ResultSet result1 = statement1.executeQuery();
            if(result1.next()) {
                System.out.println(result1.getString("email"));
                result1.close();
                statement1.close();
                connection.close();
                return -1;
            }
            result1.close();
            statement1.close();
            PreparedStatement statement = connection.prepareStatement("select U.id as cnt from userprofile U order by id desc limit 1;");
            ResultSet result = statement.executeQuery();
            if(result.next()) {
                id = result.getInt("cnt") + 1;
            }
            result.close();
            statement.close();
            PreparedStatement pStatement = connection.prepareStatement(
                    "insert into userprofile (id, firstname, lastname, phonenumber, email, password, credit, cartrestaurant) " +
                            "values (?, ?, ?, ?, ?, ?, ?, ?);");
            pStatement.setInt(1, id);
            pStatement.setString(2, firstName);
            pStatement.setString(3, lastName);
            pStatement.setString(4, phone);
            pStatement.setString(5, email);
            pStatement.setInt(6, password);
            pStatement.setInt(7, 0);
            pStatement.setString(8, "");
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -2;
    }

    public void setUserCartRestaurant(int userId, String resId) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "update userprofile set cartrestaurant = ? where id = ? ;");
            pStatement.setString(1, resId);
            pStatement.setInt(2, userId);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeCartItems(int userId) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "delete from cartitem where userId = ? ;");
            pStatement.setInt(1, userId);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeCartItem(int userId, int id) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "delete from cartitem where userId = ? and id = ? ;");
            pStatement.setInt(1, userId);
            pStatement.setInt(2, id);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void decreasePartyFoodCount(String foodName, String cartRestaurant, int count) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement statement1 = connection.prepareStatement("select * from partyfood F where F.resId = ? and F.name = ? ;");
            statement1.setString(1, cartRestaurant);
            statement1.setString(2, foodName);
            ResultSet result = statement1.executeQuery();
            int initialCount = 0;
            if(result.next()) {
                initialCount = result.getInt("count");
            }
            result.close();
            statement1.close();
            PreparedStatement pStatement = connection.prepareStatement(
                    "update partyfood set count = ? where resId = ? and name = ? ;");
            pStatement.setInt(1, initialCount - count);
            pStatement.setString(2, cartRestaurant);
            pStatement.setString(3, foodName);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setDeliveryState(int status, int orderId, int userId) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "update orderstatus set status = ? where userId = ? and id = ? ;");
            pStatement.setInt(1, status);
            pStatement.setInt(2, userId);
            pStatement.setInt(3, orderId);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setCredit(int credit, int userId) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "update userprofile set credit = ? where id = ? ;");
            pStatement.setInt(1, credit);
            pStatement.setInt(2, userId);
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeFoodParty() {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement pStatement = connection.prepareStatement(
                    "delete from partyfood ;");
            pStatement.executeUpdate();
            pStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void batchUpdateRestaurants(ArrayList<Restaurant> restaurants) {
        Connection connection;
        try {
            connection = dataSource.getConnection();
            PreparedStatement preparedStatement1 = connection.prepareStatement(
                    "insert ignore into restaurant (id, name, locx, locy, logo) values (?, ?, ?, ?, ?) ;");
            PreparedStatement preparedStatement2 = connection.prepareStatement(
                    "insert ignore into food (resId, name, description, popularity, price, image, resName)" +
                            " values (?, ?, ?, ?, ?, ?, ?);");
            PreparedStatement preparedStatement3 = connection.prepareStatement(
                    "insert ignore into partyfood (resId, name, description, popularity, price, image, count, oldPrice)" +
                            " values (?, ?, ?, ?, ?, ?, ?, ?);");
//            connection.setAutoCommit(false);
            for(int i = 0; i < restaurants.size(); i++) {
                preparedStatement1.setString(1, restaurants.get(i).getId());
                preparedStatement1.setString(2, restaurants.get(i).getName());
                preparedStatement1.setInt(3, restaurants.get(i).getLocation().getX());
                preparedStatement1.setInt(4, restaurants.get(i).getLocation().getY());
                preparedStatement1.setString(5, restaurants.get(i).getLogo());
                ArrayList<Food> menu = restaurants.get(i).getMenu();
                preparedStatement1.addBatch();
                if(menu != null)
                    for(int j = 0; j < menu.size(); j++) {
                        preparedStatement2.setString(1, restaurants.get(i).getId());
                        preparedStatement2.setString(2, menu.get(j).getName());
                        preparedStatement2.setString(3, menu.get(j).getDescription());
                        preparedStatement2.setFloat(4, menu.get(j).getPopularity());
                        preparedStatement2.setInt(5, menu.get(j).getPrice());
                        preparedStatement2.setString(6, menu.get(j).getImage());
                        preparedStatement2.setString(7, restaurants.get(i).getName());
                        preparedStatement2.addBatch();
                    }
                ArrayList<PartyFood> partyMenu = restaurants.get(i).getPartyMenu();
                if(partyMenu != null)
                    for(int j = 0; j < partyMenu.size(); j++) {
                        NikiveryRepository.getInstance().addPartyFood(restaurants.get(i).getId(), partyMenu.get(j));
                        preparedStatement3.setString(1, restaurants.get(i).getId());
                        preparedStatement3.setString(2, partyMenu.get(j).getName());
                        preparedStatement3.setString(3, partyMenu.get(j).getDescription());
                        preparedStatement3.setFloat(4, partyMenu.get(j).getPopularity());
                        preparedStatement3.setInt(5, partyMenu.get(j).getPrice());
                        preparedStatement3.setString(6, partyMenu.get(j).getImage());
                        preparedStatement3.setInt(7, partyMenu.get(j).getCount());
                        preparedStatement3.setInt(8, partyMenu.get(j).getOldPrice());
                        preparedStatement3.addBatch();
                    }
            }
//            int[] results = statement.executeBatch();
            preparedStatement1.executeBatch();
            preparedStatement1.close();
            preparedStatement2.executeBatch();
            preparedStatement2.close();
            preparedStatement3.executeBatch();
            preparedStatement3.close();
//            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<RestaurantDAO> getSearchRestaurants(String foodName, String resName, int from, int offset) throws SQLException {
        ArrayList<RestaurantDAO> restaurants = new ArrayList<RestaurantDAO>();
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select R.* from restaurant R, food F where R.name like ? and F.name " +
                "like ? and F.resId = R.id group by id limit ? , ? ;");
        statement.setString(1, "%" + resName + "%" );
        statement.setString(2, "%" + foodName + "%" );
        statement.setInt(3, from);
        statement.setInt(4, offset );
        ResultSet result = statement.executeQuery();
        while(result.next()) {
            RestaurantDAO restaurantDAO = new RestaurantDAO();
            restaurantDAO.setId(result.getString("id"));
            restaurantDAO.setName(result.getString("name"));
            Location resLoc = new Location();
            resLoc.setX(result.getInt("locx"));
            resLoc.setY(result.getInt("locy"));
            restaurantDAO.setLocation(resLoc);
            restaurantDAO.setLogo(result.getString("logo"));
            restaurants.add(restaurantDAO);
        }
        result.close();
        statement.close();
        connection.close();
        return restaurants;
    }

    public int login(String email, int password) throws SQLException {
        int userId = -1;
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select * from userprofile U where U.email = ? and U.password = ? ;");
        statement.setString(1, email);
        statement.setInt(2, password);
        ResultSet result = statement.executeQuery();
        if(result.next()) {
            userId = result.getInt("id");
        }
        result.close();
        statement.close();
        connection.close();
        return userId;
    }

    public int googleLogin(String email) throws SQLException {
        int userId = -1;
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement("select * from userprofile U where U.email = ? ;");
        statement.setString(1, email);
        ResultSet result = statement.executeQuery();
        if(result.next()) {
            userId = result.getInt("id");
        }
        result.close();
        statement.close();
        connection.close();
        return userId;
    }
}
