package org.fn.ie.repository;

public class UserProfileDAO {
    private int id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private int credit;
    private String cartRestaurant;

    public String getCartRestaurant() {
        return cartRestaurant;
    }

    public void setCartRestaurant(String cartRestaurant) {
        this.cartRestaurant = cartRestaurant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }
}
